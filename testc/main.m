//
//  main.m
//
//  Created by Artem Ustimov on 10/28/17.
//  Copyright © 2017 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DegreeItem: NSObject

@property (nonatomic) int degree;
@property (nonatomic) int firstIndex;
@property (nonatomic) int lastIndex;

@end

@implementation DegreeItem

@end

@interface Main : NSObject

- (NSNumber *)ironMan:(NSArray *)array;
- (NSNumber *)degreeOfArray:(NSArray *)array;

@end

@implementation Main

- (NSNumber *)ironMan:(NSArray *)array {
    if (array == nil || array.count == 0) {
        return [NSNumber numberWithInt:1];
    }
    int min = [array[0] intValue];
    int sum = 0;
    for (NSNumber * number in array) {
        sum += [number intValue];
        if (sum < min) {
            min = sum;
        }
    }
    if (min < 0) {
        return [NSNumber numberWithInt:-min + 1];
    } else {
        return [NSNumber numberWithInt:1];
    }
}

- (NSNumber *)degreeOfArray:(NSArray *)arr {
    if (arr.count == 0) {
        return [NSNumber numberWithInt:0];
    }
    NSMutableDictionary * degrees = [[NSMutableDictionary alloc] init];
    for (int i = 0; i < arr.count; i++) {
        DegreeItem * item = [degrees objectForKey:arr[i]];
        if (item) {
            item.degree += 1;
            item.lastIndex = i;
        } else {
            item = [[DegreeItem alloc] init];
            item.degree = 1;
            item.firstIndex = i;
            item.lastIndex = i;
            [degrees setValue:item forKey:arr[i]];
        }
    }
    int maxDegree = 1;
    int maxLength = INT_MAX;
    for (NSNumber * number in degrees) {
        DegreeItem * item = degrees[number];
        if ((item.degree > maxDegree) || (item.degree == maxDegree && maxLength > item.lastIndex - item.firstIndex)) {
            maxDegree = item.degree;
            maxLength = item.lastIndex - item.firstIndex + 1;
        }
    }
    return [NSNumber numberWithInt:maxLength];
}

@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Main * m = [[Main alloc] init];
        NSLog(@"IronMan: %@", [m ironMan:@[@-5, @4, @-2, @3, @1, @-1, @-6, @-1, @0, @5]]);
        NSLog(@"DegreeOfArray: %@", [m degreeOfArray:@[@6, @1, @1, @2, @1, @2, @2]]);
    }
    return 0;
}
